package controllers;
import java.util.Set;

import javax.inject.Inject;

import models.Book;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import views.html.*;

public class BooksController extends Controller{
	
	@Inject
	FormFactory formFactory;
	
	//Mostrar todos los libros al usuario.
	public Result index() {
		Set<Book> books = Book.allBooks();
		return ok(indexOfBooks.render(books));
	}
	
	//El usuario crea un libro.
	public Result create() {
		Form<Book> bookForm = formFactory.form(Book.class);
		
		return ok(create.render(bookForm));
	}
	
	//Para guardar un libro.
	public Result save() {
		Form<Book> bookForm = formFactory.form(Book.class).bindFromRequest();
		Book book = bookForm.get();
		Book.add(book);
		return redirect(routes.BooksController.index());
	}
	
	//Para editar un libro.
	public Result edit(Integer id) {
		Book book = Book.findById(id);
		if (book == null) {
			return notFound("Book not found");
		}
		Form<Book> bookForm = formFactory.form(Book.class).fill(book);
		return ok(edit.render(bookForm));
	}
	
	//Actualizar el libro que se pasa en edit.
	public Result update() {
		Book book = formFactory.form(Book.class).bindFromRequest().get();
		Book oldbook = Book.findById(book.id);
		if (oldbook == null) {
			return notFound("Book Not Found");
		}
		oldbook.title = book.title;
		oldbook.author = book.author;
		oldbook.price = book.price;
		
		
		return redirect(routes.BooksController.index());
	}
	
	//Detalles de un libro en particular.
	public Result show(Integer id) {
		Book book = Book.findById(id);
		if (book==null) {
			return notFound("Book not found");
		}
		return ok(show.render(book));
	}
	
	//Borrar un libro.
	public Result destroy(Integer id) {
		Book book = Book.findById(id);
		if (book == null) {
			return notFound("Book not found!");
		}
		Book.remove(book);
		return redirect(routes.BooksController.index());
	}
}
